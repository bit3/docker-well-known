#!/bin/sh
set -e

if [ "$1" = 'nginx' ]; then
  generate-well-known -root /html -config /etc/nginx/conf.d/default.conf
fi

exec "$@"