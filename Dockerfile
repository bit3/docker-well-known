FROM golang:alpine AS builder

COPY src /src
COPY docker-entrypoint.sh /bin/docker-entrypoint.sh

RUN set -xe; \
    cd /bin; \
    go build /src/generate-well-known.go

FROM nginx:alpine

COPY --from=builder /bin /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 80
STOPSIGNAL SIGTERM
CMD ["nginx", "-g", "daemon off;"]