package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type WellKnown struct {
	Path string
	Content string
	ContentType string
}

func main() {
	var err error
	var htmlRootPath string
	var nginxConfigPath string

	flag.StringVar(&htmlRootPath, "root", ".", "The html root path")
	flag.StringVar(&nginxConfigPath,"config", "default.conf", "The nginx config path")
	flag.Parse()

	htmlRootPath, err = filepath.Abs(htmlRootPath)
	if nil != err {
		log.Fatalln(err)
	}

	nginxConfigPath, err = filepath.Abs(nginxConfigPath)
	if nil != err {
		log.Fatalln(err)
	}

	wellKnownPath := filepath.Join(htmlRootPath, ".well-known")

	defs := make(map[string]*WellKnown)

    for _, env := range os.Environ() {
        pair := strings.SplitN(env, "=", 2)
        if strings.HasPrefix(pair[0], "WK_") {
        	name := strings.TrimSpace(pair[0])
        	name = strings.TrimPrefix(name, "WK_")
        	name = strings.TrimSuffix(name, "__PATH")
        	name = strings.TrimSuffix(name, "__CONTENT_TYPE")

        	value := strings.TrimSpace(pair[1])

        	wk, exists := defs[name]

        	if !exists {
        		wk = &WellKnown{
					Path:        strings.ToLower(strings.ReplaceAll(name, "__", "/")),
					Content:     "",
					ContentType: "",
				}
				defs[name] = wk
			}

        	if strings.HasSuffix(pair[0], "__PATH") {
        		if strings.Contains(value, "/..") || strings.Contains(value, "../") {
        			log.Fatalf("Invalid path %s, path iteration is prohibited", env)
				}

        		if strings.Contains(value, "//") || strings.Contains(value, "/./") {
        			log.Fatalf("Invalid path %s, path not normalized", env)
				}

        		if strings.HasSuffix(value, "/") {
        			log.Fatalf("Invalid path %s, must end with filename", env)
				}

				wk.Path = strings.TrimLeft(value, "/")
			} else if strings.HasSuffix(pair[0], "__CONTENT_TYPE") {
				wk.ContentType = value
			} else {
				wk.Content = value
			}
		}
    }

	log.Println("well-known entries:")

	for _, wk := range defs {
		log.Printf("  %s\n", wk.Path)
		log.Printf("    ContentType: %s\n", wk.ContentType)
		log.Printf("    Content: %s\n", wk.Content)
	}
	log.Println()

    log.Printf("Creating %s contents\n", htmlRootPath)

    err = os.MkdirAll(wellKnownPath, 0755)
    if nil != err && !os.IsExist(err) {
    	log.Fatalln(fmt.Sprintf("Failed to create %s directory", wellKnownPath), err)
	}

    for _, wk := range defs {
		path := filepath.Join(wellKnownPath, wk.Path)
		dir := filepath.Dir(path)

		err = os.MkdirAll(dir, 0755)
		if nil != err {
			log.Fatalln(fmt.Sprintf("Failed to create directory %s", dir), err)
		}

		file, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
		if nil != err {
			log.Fatalln(fmt.Sprintf("Failed to open file %s", path), err)
		}

		_, err = file.WriteString(wk.Content)
		if nil != err {
			log.Fatalln(fmt.Sprintf("Failed to write file %s", path), err)
		}

		err = file.Close()
		if nil != err {
			log.Fatalln(fmt.Sprintf("Failed to close file %s", path), err)
		}
	}

	log.Printf("Creating %s nginx config\n", nginxConfigPath)

	var builder strings.Builder

    builder.WriteString(`server {
    listen       80;
    server_name  localhost;

    charset utf8;

    add_header Content-Security-Policy "default-src 'self'";
    add_header X-Frame-Options "sameorigin";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";
    server_tokens off;

    location / {
		limit_except GET {
		  deny all;
		}

        root   ` + htmlRootPath + `;
        index  index.html index.htm;
        default_type text/plain;
`)

	for _, wk := range defs {
		if "" != wk.ContentType {
			builder.WriteString(`
        location /.well-known/` + wk.Path + ` {
            types        { }
            default_type ` + wk.ContentType + `;
        }
`)
		}
	}

    builder.WriteString(`
    }
}`)

	file, err := os.OpenFile(nginxConfigPath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if nil != err {
		log.Fatalln(fmt.Sprintf("Failed to open file %s", nginxConfigPath), err)
	}

	_, err = file.WriteString(builder.String())
	if nil != err {
		log.Fatalln(fmt.Sprintf("Failed to write file %s", nginxConfigPath), err)
	}

	err = file.Close()
	if nil != err {
		log.Fatalln(fmt.Sprintf("Failed to close file %s", nginxConfigPath), err)
	}
}
