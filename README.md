# Static `.well-known` made easy

[![gitlab.com/bit3/docker-well-known](https://img.shields.io/badge/gitlab.com-bit3%2Fdocker--well--known-red?logo=gitlab)](https://gitlab.com/bit3/docker-well-known)
[![hub.docker.com/r/bit3/well-known](https://img.shields.io/badge/Docker%20Hub-bit3%2Fwell--known-blue?logo=docker)](https://hub.docker.com/r/bit3/well-known)

Provides an environment based `.well-known` service for static content.
Reads `WK_*` variables to generate a static `.well-known` content.

## Environment Variable EBNF Grammar

```
WellKnownVariable   = ContentVariable
                    | PathVariable
                    | ContentTypeVariable
ContentVariable     = Prefix, Name
PathVariable        = Prefix, Name, "__PATH"
ContentTypeVariable = Prefix, Name, "__CONTENT_TYPE"
Prefix              = "WK_"
Name                = Alnum, {Sign}
Sign                = Alnum | "_"
Alnum               = Character | Digit
Character           = "A" | "B" | "C" | "D" | "E" | "F" | "G"
                    | "H" | "I" | "J" | "K" | "L" | "M" | "N"
                    | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
                    | "V" | "W" | "X" | "Y" | "Z" ;
Digit               = "0" | "1" | "2" | "3" | "4" | "5" | "6"
                    | "7" | "8" | "9" ;
```

## Examples

### dnt-policy.txt

No need to specify content type, if file extension is known by nginx.

```shell script
docker run --rm -it \
       --env=WK_DNT_POLICY='Do Not Track Compliance Policy

Version 1.0

This domain complies with user opt-outs from tracking via the "Do Not Track"
or "DNT" header  [http://www.w3.org/TR/tracking-dnt/].  This file will always
be posted via HTTPS at https://example-domain.com/.well-known/dnt-policy.txt
to indicate this fact.

SCOPE
...
' \
       --env=WK_DNT_POLICY___PATH=dnt-policy.txt \
       bit3/well-known:latest
```

#### Test case

```shell script
CONTAINER_ID=$(docker ps --no-trunc | grep bit3/well-known:latest | awk '{ print $1; }')
CONTAINER_IP_ADDR=$(docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_ID)
curl -i "http://$CONTAINER_IP_ADDR/.well-known/dnt-policy.txt"
```

### Matrix Client

If `__PATH` is omited, the name `MATRIX_CLIENT` will be transformed to `matrix/client`.

```shell script
docker run --rm -it \
       --env=WK_MATRIX__CLIENT='{
    "m.homeserver": {
        "base_url": "https://matrix-client.matrix.org"
    },
    "m.identity_server": {
        "base_url": "https://vector.im"
    }
}' \
       --env=WK_MATRIX__CLIENT___CONTENT_TYPE=application/json \
       bit3/well-known:latest
```

#### Test case

```shell script
CONTAINER_ID=$(docker ps --no-trunc | grep bit3/well-known:latest | awk '{ print $1; }')
CONTAINER_IP_ADDR=$(docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $CONTAINER_ID)
curl -i "http://$CONTAINER_IP_ADDR/.well-known/matrix/client"
```
